﻿using System.Globalization;

namespace EVEMon.Common
{
    public static class CultureConstants
    {
        public static readonly CultureInfo DefaultCulture = CultureInfo.CurrentCulture;
        public static readonly CultureInfo InvariantCulture = CultureInfo.InvariantCulture;
    }
}